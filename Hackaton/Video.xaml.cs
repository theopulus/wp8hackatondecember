﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Hackaton
{
    public partial class Video : PhoneApplicationPage
    {
        public Video()
        {
            InitializeComponent();
            video.Source = App.VideoToShow;
        }
    }
}