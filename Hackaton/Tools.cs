﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Windows.Phone.Speech.Synthesis;


namespace Hackaton
{
    class Tools
    {
        public static void pDebug(String strMessage)
        {
            #if Debug 
            System.Diagnostics.Debug.WriteLine (strMessage); 
            #endif

        }

        public static async void tts(String me)
        {
            if (string.IsNullOrEmpty(me))
                MessageBox.Show("Por favor, introduzca un texto para ser leído.");
            else
            {
                try
                {
                    SpeechSynthesizer synth = new SpeechSynthesizer();

                    var voices = InstalledVoices.All.Where(v => v.Language == "es-ES").OrderByDescending(v => v.Gender);

                    VoiceGender gender = VoiceGender.Female;
                    //gender = VoiceGender.Male;

                    //if (rbMale.IsChecked == true)
                    //gender = VoiceGender.Male;
                    //else
                    //gender = VoiceGender.Female;

                    synth.SetVoice(voices.Where(v => v.Gender == gender).FirstOrDefault());
                    try
                    {
                        await synth.SpeakTextAsync(me);
                    }
                    catch (OperationCanceledException ex)
                    {
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        public static void tts_stop()
        {
            SpeechSynthesizer synth = new SpeechSynthesizer();

            synth.CancelAll();
        }
    }
}
