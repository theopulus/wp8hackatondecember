﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Hackaton.Resources;

namespace Hackaton
{
    public partial class MainPage : PhoneApplicationPage
    {

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            btnShowData.IsEnabled = false;
        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            SystemTray.ProgressIndicator = new ProgressIndicator();
            if (App.firstTime1)
                Tools.tts("Toque sobre los textos descriptivos para escucharlos en voz.");
        }

        private static void SetProgressIndicator(bool isVisible)
        {
            SystemTray.ProgressIndicator.Text = isVisible ? "Obteniendo Información..." : "";
            SystemTray.ProgressIndicator.IsIndeterminate = isVisible;
            SystemTray.ProgressIndicator.IsVisible = isVisible;
            //SystemTray.IsVisible = isVisible;
            
        }

        private void StorePaintingInfo()
        {
            Tools.pDebug("Storing data on Model");
        }

        private async void btnRetrieve_Click(object sender, RoutedEventArgs e)
        {
            SetProgressIndicator(true);

            btnShowData.IsEnabled = false;

            JsonWebClient client = new JsonWebClient();
            var resp = await client.DoRequestJsonAsync<MPainting>("http://hackaton.applicarte.es/images/hackaton/");

            App.PaintingData = (MPainting)resp;

            btnShowData.IsEnabled = true;
            SetProgressIndicator(false);
            if (App.firstTime1)
            {
                Tools.tts(txtSecond.Text);
                App.firstTime1 = false;
            }
        }

        private void btnShowData_Click(object sender, RoutedEventArgs e)
        {
            Tools.tts_stop();
            NavigationService.Navigate(new Uri("/InformationData.xaml", UriKind.Relative));
        }

        private void TextBlock_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            TextBlock me = (TextBlock)sender;

            Tools.tts(me.Text);
        }

    }
}