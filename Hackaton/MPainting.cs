﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Hackaton
{
    public class MResource
    {
        public string title { get; set; }
        public string resource_file { get; set; }
        public string resource_object { get; set; }
        public string type_of_resource { get; set; }
        public string description { get; set; }
        public Uri uri { get; set; }
        public BitmapImage image { get; set; }

    }

    public class MMetadataPainting
    {
        public string scope { get; set; }
        public string technique { get; set; }
        public string partner { get; set; }
        public string big_description { get; set; }
    }

    public class MMetadataImage
    {
        public string author { get; set; }
        public string short_description { get; set; }
        public string year_of_creation { get; set; }
        public string title { get; set; }
    }

    public class MPainting
    {
        public List<MResource> all_resources { get; set; } 
        public string image_file { set;  get; }
        public string image_file_url { set; get; }
        public MMetadataPainting metadata_painting { get; set; }
        public string id { get; set; }
        public MMetadataImage metadata_image { get; set; }
    }
}
