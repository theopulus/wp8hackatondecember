﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Hackaton
{
    public partial class ImageShow : PhoneApplicationPage
    {
        public ImageShow()
        {
            InitializeComponent();
            imgElement.Source = App.ImageToShow;
        }

        private void imgElement_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.GoBack();
        }
    }
}