﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using Windows.Phone.Speech.Synthesis;
using System.Windows.Media;


namespace Hackaton
{
    public partial class InformationData : PhoneApplicationPage
    {
        MPainting paintingData { get; set; }
        List<MResource> videos { get; set; }
        List<MResource> audios { get; set; }
        List<MResource> pictures { get; set; }
        

        public InformationData()
        {
            InitializeComponent();
            prepareData();
            this.DataContext = paintingData;
            FillComponents();
        }

        void prepareData()
        {
            videos = new List<MResource>();
            audios = new List<MResource>();
            pictures = new List<MResource>();

            paintingData = App.PaintingData;
            foreach (MResource item in paintingData.all_resources)
            {
                Uri uri = new Uri("http://hackaton.applicarte.es/media/" + item.resource_object, UriKind.RelativeOrAbsolute);
                
                switch (item.type_of_resource)
                {
                    case "IM":
                        item.image = new BitmapImage(uri);
                        pictures.Add(item);
                        break;
                    case "AU":
                        item.uri = uri;
                        audios.Add(item);
                        break;
                    case "VI":
                        item.uri = uri;
                        videos.Add(item);
                        break;
                }
            }
        }

        void FillComponents()
        {
            Uri uri = new Uri("http://hackaton.applicarte.es/" + paintingData.image_file_url, UriKind.Absolute);
            panDataBrush.ImageSource = new BitmapImage(uri);

            llsVideos.ItemsSource = videos;
            llsImages.ItemsSource = pictures;
            llsAudios.ItemsSource = audios;
        }

        private async void TextBlock_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            TextBlock me = (TextBlock)sender;

            if (string.IsNullOrEmpty(me.Text))
                MessageBox.Show("Por favor, introduzca un texto para ser leído.");
            else
            {
                try
                {
                    SpeechSynthesizer synth = new SpeechSynthesizer();

                    var voices = InstalledVoices.All.Where(v => v.Language == "es-ES").OrderByDescending(v => v.Gender);

                    VoiceGender gender = VoiceGender.Female;
                    //gender = VoiceGender.Male;

                    //if (rbMale.IsChecked == true)
                    //gender = VoiceGender.Male;
                    //else
                    //gender = VoiceGender.Female;

                    synth.SetVoice(voices.Where(v => v.Gender == gender).FirstOrDefault());

                    await synth.SpeakTextAsync(me.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void MediaElement_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            MediaElement me = (MediaElement)sender;

            
            App.VideoToShow = (Uri)me.Source;

            NavigationService.Navigate(new Uri("/Video.xaml", UriKind.Relative));
        }

        private void MediaElement_DoubleTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            MediaElement me = (MediaElement)sender;
        }

        private void Play_Click(object sender, RoutedEventArgs e)
        {
            Button me = (Button)sender;
            playMedia(me, 0);

            
        }

        private void playMedia(FrameworkElement me, int action)
        {
            Grid grid = (Grid)me.Parent;

            UIElementCollection children = grid.Children;
            foreach (FrameworkElement item in children)
            {
                if (item.GetType() == typeof(MediaElement))
                {
                    MediaElement mediaElement = (MediaElement)item;
                    switch (action)
                    {
                        case 0:
                            mediaElement.Play();
                            break;
                        case 1:
                            mediaElement.Pause();
                            break;
                    }
                }
            }
            
        }

        private void Pause_Click(object sender, RoutedEventArgs e)
        {
            Button me = (Button)sender;
            playMedia(me, 1);
        }

        private void Border_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Border me = (Border)sender;

            ImageBrush imgBrush = (ImageBrush)me.Background;
            App.ImageToShow = (BitmapImage)imgBrush.ImageSource;
            
            NavigationService.Navigate(new Uri("/ImageShow.xaml", UriKind.Relative));
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (App.firstTime2)
            {
                string instructions = "Esta pantalla muestra varias secciones que podrá visitar deslizando con el dedo sobre la pantalla en ambas direcciones.\r\nLas imágenes se pueden ver en pantalla completa tocando sobre ellas; los vídeos y sonidos disponen de controles de Reproducción y Pausa.";
                Tools.tts(instructions);
                MessageBox.Show(instructions);

                App.firstTime2 = false;
            }
        }
    }

}